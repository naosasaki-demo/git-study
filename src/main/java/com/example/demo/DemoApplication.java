package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@PostMapping("/")
	String home() {
		return "Spring is here!";
	}
	// hot fix
	// commit 2
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	// commit 3
	// commit 4
	// commit 5
}
